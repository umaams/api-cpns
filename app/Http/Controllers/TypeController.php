<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TypeController extends Controller
{
    public function index (Request $request)
    {
    	$query = DB::table('formasi')
        ->select('JENIS_FORMASI_NM')->addSelect(DB::raw('SUM(JUM_PERJAB) AS TOTAL_FORMASI'))
        ->groupBy('JENIS_FORMASI_NM');
        $data['types'] = $query->get();
        return response()->json(compact('data'));
    }
}
