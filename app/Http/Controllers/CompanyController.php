<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CompanyController extends Controller
{
    public function index (Request $request)
    {
        $query = DB::table('formasi')
        ->select('INS_NM')->addSelect(DB::raw('SUM(JUM_PERJAB) AS TOTAL_FORMASI'))
        ->groupBy('INS_NM');
        $data['companies'] = $query->get();
        return response()->json(compact('data'));
    }
}
