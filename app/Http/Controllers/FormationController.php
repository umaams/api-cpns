<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FormationController extends Controller
{
    public function index (Request $request)
    {
        $query = DB::table('formasi');
        if ($request->has('company')) {
        	$query->where('INS_NM', $request->get('company'));
        }
        if ($request->has('education')) {
        	$query->where('PENDIDIKAN_NM', $request->get('education'));
        }
        if ($request->has('type')) {
        	$query->where('JENIS_FORMASI_NM', $request->get('type'));
        }
        $data['formations'] = $query->get();
        return response()->json(compact('data'));
    }
}
