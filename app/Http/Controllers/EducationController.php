<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class EducationController extends Controller
{
    public function index (Request $request)
    {
    	$query = DB::table('formasi')
        ->select('PENDIDIKAN_NM')->addSelect(DB::raw('SUM(JUM_PERJAB) AS TOTAL_FORMASI'))
        ->groupBy('PENDIDIKAN_NM');
        $data['educations'] = $query->get();
        return response()->json(compact('data'));
    }
}
